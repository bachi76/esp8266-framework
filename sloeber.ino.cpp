#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2017-11-30 20:14:02

#include "Arduino.h"
#include <ArduinoJson.h>
#include <Firebase.h>
#include <Log.h>
#include <Config.h>
#include <IotServer.h>
#include <Test.h>
#include "Framework/WebComponents/src/WC_Routes.h"
#include "Framework/WebComponents/src/ESP8266WebServerInspectable.h"
#include "Framework/DynDNS/DynDNS.h"

void setup() ;
void runTests() ;
void loop() ;
void testLog() ;
void testDynDNS() ;
void testFirebase() ;
void testConfig() ;
bool connectToWifi(String ssid, String password) ;
void mem() ;
const char* wifi_ssid() ;
const char* wifi_pw() ;
String dynDnsBasicAuth() ;

#include "ESP_Framework.ino"

#include "local_conf.ino"
#include "local_conf_dist.ino"

#endif
