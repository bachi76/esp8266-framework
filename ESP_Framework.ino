/*
 * ESP_Framework.ino
 *
 * Tests for the ESP Framework libraries. Check out the examples below for library usage hints.

 * Notes:
 *
 * 1) Rename local_conf_dist.ino to local_conf.ino and fill in your wifi connection settings.
 *
 * 2) Symlink (or copy) the folders in /Framework to your Arduino library folder.
 *
 *
 * @author Bachi
 */

#include <ArduinoJson.h>
#include <Firebase.h>
#include <Log.h>

#include <Config.h>
#include <IotServer.h>
#include <Test.h>
#include "Framework/WebComponents/src/WC_Routes.h"
#include "Framework/WebComponents/src/ESP8266WebServerInspectable.h"
#include "Framework/DynDNS/DynDNS.h"

const char* TAG = "Main";

void setup() {

	mem();

	Serial.begin(115200);
	pinMode(BUILTIN_LED, OUTPUT);


	// Need internet connection!
	while (!connectToWifi(wifi_ssid(), wifi_pw())) {
		delay(2000);
		Log::w(TAG, ".. retrying connection.");
	}

	// Single test run
	runTests();
}

void runTests() {

	Log::i(TAG, "*** Starting tests ***");

	// Execute tests

	//	testLog();
	//	mem();

	//	testFirebase();
	//	mem();

	testConfig();
	mem();

	//	testDynDNS();
	//	mem();

	//	TODO testIotServer();
	mem();

	//	TODO testWebSocketHandler();
	mem();

	Log::i(TAG, "\n*** Finished all tests ***");
}


void loop() {

	// For repeated tests
	// Note: Don't run repeated tests for DynDNS (flood blocking) or Config (flash write cycles)

	//	mem();
	//	runTests();
	//	Log::d(TAG, "Memory after leaving runTests()");
	//	mem();
	//	Serial.println("-------------------\n\n\n");
	//	delay(3000);
}


void testLog() {

	Log::i(TAG, "\nStarting Log test");

	Log::enabled = true;

	Log::d(TAG, "A debug msg");
	Log::i(TAG, "An info msg");
	Log::w(TAG, "A warning msg");
	Log::e(TAG, "An error msg");

	Log::enabled = false;

	Log::e(TAG, "SHOULD NOT BE VISIBLE!");

	Log::enabled = true;

}

void testDynDNS() {
	TestSuite t = TestSuite("DynDNS");
	Log::i(TAG, "Starting DynDNS test");

	String host = "esp.bachi.ch";
	String ip = WiFi.localIP().toString();

	t.test("Update host", DynDNS::update(host, ip, dynDnsBasicAuth()));

}


void testFirebase() {

	TestSuite t = TestSuite("Firebase");
	Log::i(TAG, "\nStarting Firebase test");


	Firebase fb("esp-framework-test.firebaseio.com", "testpath");

	String keyVal1 = fb.keyVal("putkey1", "putvalue1");
	String keyVal2 = fb.keyVal("putkey2", "putvalue2");

	// Initially use the longest string params to prevent memory fragmentation(?)
	t.test("Add to list", fb.addRow("somelist-and-some-extra-long-string-here", "{\"name\":\"value-with-some-extra-long-string\"}").success && fb.addRow("somelist", "{\"name\":\"value\"}").success);


	t.test("Cannot delete root", !fb.remove("").success);

	t.test("Drop DB", fb.dropDB().success);

	t.test("Set", fb.setNode("data", keyVal1).success);

	t.test("Get", fb.getNode("data").data == keyVal1);

	t.testString("GetSingleValue", fb.getValue("data/putkey1").data, "putvalue1");

	t.test("SetValue with path 1", fb.setValue("node1/node2/subkey", "subvalue").success && fb.getValue("node1/node2/subkey").data.equals("subvalue"));
	t.test("SetValue with path 2", fb.setValue("/node3/node4/subkey", "subvalue").success && fb.getValue("node3/node4/subkey").data.equals("subvalue"));

	t.test("Remove", fb.remove("data").success && !fb.getNode("data").success);

	t.test("setValue", fb.setValue("myKey", "myValue").success);
	t.testString("getValue", fb.getValue("myKey").data, "myValue");

	t.test("Add to list", fb.addRow("somelist", "{\"name\":\"value\"}").success && fb.addRow("somelist", "{\"name\":\"value\"}").success);

	//	Log::i(TAG, "somelist contains: " + fb.getNode("somelist").data);

	//if (!fb.get("somelist") == "{???}") Log::e(TAG, "List adds failed");

	t.printReport();

}

void testConfig() {

	TestSuite t = TestSuite("Config");
	Log::i(TAG, "\nStarting Config test");


	class MyConfig : public Config {

	public:

		MyConfig() : Config() {};


		char* keyChar = "initial";
		int keyInt = 7;
		String keyString = "initial";

		void onLoad(JsonObject &json) {

			if (json.containsKey("keyChar")) keyChar =  strcpy(keyChar, json["keyChar"]);
			if (json.containsKey("keyInt")) keyInt = json["keyInt"];
			if (json.containsKey("keyString")) keyString = String(json["keyString"].asString());
		}

		void onSave(JsonObject &json) {

			json["keyChar"] = keyChar;
			json["keyInt"] = keyInt;
			json["keyString"] = keyString;

		}
	};


	String configFile = "/testconfig.json";

	// Format the FS first  - sometimes needed if opening files for writing fails
	//	Log::d(TAG, "Formatting spiffs..");
	//	if (!SPIFFS.format()) {
	//		Log::w(TAG, "Formatting failed");
	//	}

	// Removing previous config file if present
	SPIFFS.begin();
	SPIFFS.remove(configFile);


	MyConfig cfg = MyConfig();
	cfg.loadFromFS(configFile);
	t.testString("Default char*", cfg.keyChar, "initial");
	t.testInt("Default int", cfg.keyInt, 7);
	t.testString("Default String", cfg.keyString, "initial");

	cfg.keyChar = "updated";
	cfg.keyString = "updated";
	cfg.keyInt = 11;

	cfg.save();

	MyConfig cfg2 = MyConfig();
	cfg2.loadFromFS(configFile);

	t.testString("Stored char*", cfg2.keyChar, "updated");
	t.testInt("Stored int", cfg2.keyInt, 11);
	t.testString("Stored String", cfg2.keyString, "updated");


	t.printReport();

}


bool connectToWifi(String ssid, String password) {

	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid.c_str(), password.c_str());
	int i = 0;
	while (WiFi.status() != WL_CONNECTED && i < 10) {
		i++;
		delay(1000);
		Serial.print(".");
	}

	if (WiFi.status() == WL_CONNECTED) {
		Serial.println("");
		Serial.println("WiFi connected");
		Serial.println("IP address: ");
		Serial.println(WiFi.localIP());
		return true;

	} else {
		Serial.println("");
		Serial.println("Could not connect to wifi.");
		return false;
	}
}


void mem() {
	Log::i(TAG, "Free heap: " + String(ESP.getFreeHeap()));
}
