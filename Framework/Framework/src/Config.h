/*
 * Configuration base class to handle persistent config values.
 * Loads from and stores to flash memory using the SPIFFS filesystem.
 *
 * Data is stored in Json format. Multiple config files are supported.
 * To use, subclass it and overwrite onLoad() / onSave() to extract / add
 * config values from/to the passed json object.
 *
 *
 *  Created on: 27.11.2015
      Author: bachi
 */

#ifndef FRAMEWORK_CONFIG
#define FRAMEWORK_CONFIG

#include <ArduinoJson.h>
#include <FS.h>
#include <Log.h>

class Config {

public:

	/**
	 * The implementating class extracts the confic value
	 */
	virtual void onLoad(JsonObject &json){};

	/**
	 * The implementing class puts the confic values to the json object
	 */
	virtual void onSave(JsonObject &json){};


	/**
	 * The default load method loads the /config.json file from SPIFFS file system
	 */
	JsonObject& load() {
		return loadFromFS(_configPath);
	}


	/**
	 * Load a config file from the file system.
	 */
	JsonObject& loadFromFS(String filePath) {

		if (!SPIFFS.begin()) {
			Log::e(TAG, "Failed to mount file system");
		}

		File configFile = SPIFFS.open(filePath, "r");
		if (!configFile) {
			Log::w(TAG, "Failed to open config file");
		}

		size_t size = configFile.size();

		// FIXME: Still required?
		if (size > 1024) {
			Log::w(TAG, "Config file size is too large (> 1024)");
		}

		String fcontent = configFile.readString();
		Log::d(TAG, "Config file loaded: " + fcontent);

		//DynamicJsonBuffer jsonBuffer;
		JsonObject& json = jsonBuffer.parseObject(fcontent);

		if (!json.success()) {
			Log::w(TAG, "Failed to parse config file");
		}

		_configPath = filePath;

		// Where the actual json->attribute mapping is done.
		onLoad(json);

		return json;

	}

	/**
	 * Load config values from the passed json config string
	 * On success, onLoad is called with the parsed json.
	 *
	 */
	JsonObject& loadFromJson(String& configJson) {

		//DynamicJsonBuffer jsonBuffer;
		JsonObject& json = jsonBuffer.parseObject(configJson);

		if (!json.success()) {
			Log::w(TAG, "Failed to parse passed config string.");
			// return NULL; doesnt compile
		}

		onLoad(json);
		return json;

	}

	/**
	 * Saves the config file to the file system, using the path used when loaded,
	 * default /config.json
	 */
	bool save() {
		return saveToFS(_configPath);
	}


	/**
	 * Saves the config file on the SPIFFS filesystem to the given file path.
	 */
	bool saveToFS(String& filePath) {

		//StaticJsonBuffer<1024> jsonBuffer;
		//DynamicJsonBuffer jsonBuffer;
		JsonObject& json = jsonBuffer.createObject();

		onSave(json);

		if (!SPIFFS.begin()) {
			Log::e(TAG, "Failed to mount file system (save)");
		}

		Log::d(TAG, "Opening file for writing: " + filePath);
		File configFile = SPIFFS.open(filePath, "w");
		if (!configFile) {
			Log::e(TAG, "Failed to open config file for writing. Code: " + String(configFile));
			return false;
		}

		String data = String();
		json.printTo(data);
		Log::d(TAG, "Storing this data: " + data);
		json.printTo(configFile);
		configFile.flush();
		configFile.close();

		return true;
	}


	/**
	 * Get the json representation of the config object
	 * (will call onSave() to let the concrete implementation write the state)
	 */
	JsonObject& getJson() {

		JsonObject& json = jsonBuffer.createObject();

		onSave(json);
		return json;
	}


protected:

	DynamicJsonBuffer jsonBuffer;

	const char* TAG = "Config";
	String _configPath = "/config.json";

};


#endif FRAMEWORK_CONFIG
