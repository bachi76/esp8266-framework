/*
 * Log.h
 *
 * A simple log handler, Android-style.
 * TODO: configurable output (tag and level)
 *
 *  Created on: 20.11.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_FRAMEWORK_SRC_LOG_H_
#define FRAMEWORK_FRAMEWORK_SRC_LOG_H_
#include <vector>


/* Listener callback function type */
typedef void (*LogListener)(const char*, const char*, const char*);

/**
 * Log class, allows logging to serial and remote (IotServer and WebSocketHandler clients) logging.
 */
class Log {

public:

	constexpr static const char* delim = " - ";
	constexpr static const char* DEBUG = "";
	constexpr static const char* INFO = "";
	constexpr static const char* WARN = "WARN: ";
	constexpr static const char* ERROR = "ERR: ";
	constexpr static const char* ALERT = "ALERT: ";

	static bool enabled;

	static void addListener(LogListener listener) {
		listeners.push_back(listener);
	}

	static void d(String tag, String message) {
		if (Log::enabled) {
			println(DEBUG, tag.c_str(), message.c_str());
			// Debug msgs are not remote-logged
		}
	}

	static void i(String tag, String message) {
		if (Log::enabled) {
			println(INFO, tag.c_str(), message.c_str());
			call("INFO", tag.c_str(), message.c_str());
		}
	}

	static void w(String tag, String message) {
		if (Log::enabled) {
			println(WARN, tag.c_str(), message.c_str());
			call("WARN", tag.c_str(), message.c_str());
		}
	}

	static void e(String tag, String message) {
		if (Log::enabled) {
			println(ERROR, tag.c_str(), message.c_str());
			call("ERROR", tag.c_str(), message.c_str());
		}
	}

	static void a(String tag, String message) {
		call("ALERT", tag.c_str(), message.c_str());

	}

	static void println(const char* severity, const char* tag, const char* message) {
		Serial.print(severity);
		Serial.print(delim);
		Serial.print(tag);
		Serial.print(delim);
		Serial.println(message);
	}

protected:

	static std::vector<LogListener> listeners;

	static void call(const char* severity, const char* tag, const char* msg) {
		for (auto l : listeners) {
			(*l)(severity, tag, msg);
		}
	}

};

bool Log::enabled = true;
std::vector<LogListener> Log::listeners;


#endif /* FRAMEWORK_FRAMEWORK_SRC_LOG_H_ */
