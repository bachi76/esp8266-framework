/*
 * iotServer.h
 *
 *  Created on: 25.04.2017
 *      Author: bachi
 */

#ifndef LIBRARIES_FRAMEWORK_SRC_IOTSERVER_H_
#define LIBRARIES_FRAMEWORK_SRC_IOTSERVER_H_

#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>

/**
 * Send data to the IoT server
 */
class IotServer {

public:

	// Http response code of the last request
	int responseCode = 0;

	// The url used in the last request
	String url;

	// The full response body of the last request
	String response;

	// The execution time of the last request
	unsigned long requestTime = 0;


	IotServer(const char* url, const char* token) {

		this->baseUrl = url;
		this->token = token;
		response.reserve(30);
	}

	/**
	 * Send arbitrary data to the IoT server. The 'type' param can be thought of as a table name
	 */
	int post(const char* type, JsonObject &json) {

		if (WiFi.status() != 3 /*WL_CONNECTED*/) {
			responseCode = -1;
			response = "wifi not connected.";
			requestTime = 0;
			return responseCode;
		}

		unsigned long start = millis();
		HTTPClient httpClient;
		url = baseUrl + String("/api/db/") + type + String("/");
		httpClient.begin(url);
		httpClient.addHeader("token", token);
		httpClient.addHeader("Content-Type", "application/json");

		String output;
		output.reserve(256); // TODO any use to prevent mem frag?
		json.printTo(output);
		responseCode = httpClient.POST(output);
		response = httpClient.getString();
		httpClient.end();

		requestTime = millis() - start;
		return responseCode;
	}

	/**
	 * Send a single key/value pair to the IoT server
	 */
	int post(const char* key, int value) {
		StaticJsonBuffer<200> buf;
		JsonObject& json = buf.createObject();
		json[key] = value;
		return post(key, json);
	}

	/**
	 * Send a single key/value pair to the IoT server
	 */
	int post(const char* key, double value) {
		StaticJsonBuffer<200> buf;
		JsonObject& json = buf.createObject();
		json[key] = value;
		return post(key, json);
	}

	/**
	 * Send a single key/value pair to the IoT server
	 */
	int post(const char* key, const char* value) {
		StaticJsonBuffer<200> buf;
		JsonObject& json = buf.createObject();
		json[key] = value;
		return post(key, json);
	}

	/**
	 * Write a log entry to the IoT server.
	 * You can also use Log::<method> and enable remote logging instead.
	 */
	int log(const char* severity, const char* tag, const char* msg) {

		StaticJsonBuffer<200> buf;
		JsonObject& json = buf.createObject();
		json["severity"] = severity;
		json["tag"] = tag;
		json["msg"] = msg;
		return post("log", json);
	}


protected:

	String baseUrl;
	String token;
};

#endif /* LIBRARIES_FRAMEWORK_SRC_IOTSERVER_H_ */
