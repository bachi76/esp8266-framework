/*
 * Util.h
 *
 * A place for utility functions
 *
 *  Created on: 01.12.2015
 *      Author: bachi
 */

#ifndef LIBRARIES_FRAMEWORK_SRC_UTIL_H_
#define LIBRARIES_FRAMEWORK_SRC_UTIL_H_

class Util {

public:

	/**
	 * Establish a one-time wifi connection. Returns true on connection, false if no connection
	 * could be established within 20s.
	 */
	static bool connectToWifi(String ssid, String password, WiFiMode wifiMode) {

		WiFi.mode(wifiMode);
		WiFi.begin(ssid.c_str(), password.c_str());
		int i = 0;
		while (WiFi.status() != WL_CONNECTED && i < 20) {
			i++;
			delay(1000);
			yield();
			Serial.print(".");
		}

		if (WiFi.status() == WL_CONNECTED) {
			Serial.println("");
			Serial.println("WiFi connected");
			Serial.println("IP address: ");
			Serial.println(WiFi.localIP());
			return true;

		} else {
			Serial.println("");
			Serial.println("Could not connect to wifi.");
			return false;
		}
	}

	/**
	 * Checks if the esp is still connected to the wifi, and tries to re-connect if not.
	 * Returns true on connection, false if no connection could be established within 20s.
	 * Add this to your loop() to keep connected.
	 */
	static bool keepConnected(String ssid, String password, WiFiMode wifiMode) {
		// Check wifi and try to reconnect if failed
		if (WiFi.status() == WL_CONNECTED) {
			return true;

		} else {

			if (connectToWifi(ssid, password, wifiMode)) {
				Log::w("Wifi", "Wifi reconnected.");
				return true;

			} else {
				Log::e("Wifi", "Could not reconnect to wifi.");
				return false;
			}
		}
	}
};

#endif /* LIBRARIES_FRAMEWORK_SRC_UTIL_H_ */
