/*
 * Test.h
 *
 * A minimal test framework.
 * Create a TestSuite, run tests, output the result.
 *
 *  Created on: 20.11.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_TESTSUITE
#define FRAMEWORK_TESTSUITE

#include <Log.h>

class TestSuite {

public:

	String name;
	unsigned int successful;
	unsigned int failures;
	String successLog;
	String failureLog;

	TestSuite(String name) {
		this->name = name;
		failures = 0;
		successful = 0;
		failureLog = "";
		successLog = "";
	}

	void test(String name, bool success) {
		log(success, name + "' - " + (success ? "OK" : "FAIL"));
	}

	void testString(String name, String actual, String expected) {
		bool success = actual.equals(expected);
		String msg = name + "' - " + (success ? "OK" : "FAIL");
		if (!success) {
			msg +=("  -> Should be '" + expected + "' but was '" + actual + "'");
		}

		log(success, msg);
	}

	void testInt(String name, int actual, int expected) {
			bool success = actual == expected;
			String msg = name + "' - " + (success ? "OK" : "FAIL");
			if (!success) {
				msg +=("  -> Should be '" + String(expected) + "' but was '" + String(actual) + "'");
			}

			log(success, msg);
		}


	void printReport() {
		Serial.println("\n*** Test report for '" + name + "': ***\n");
		Serial.println("* Success:");
		Serial.println(successLog);
		Serial.println();
		Serial.println("* Failed:");
		Serial.println(failureLog);
		Serial.println();
		Serial.println("TEST RESULT: " + (failures == 0 ? String("✔ SUCCESS") : String("✘ FAILURE")));
	}


private:

	const char* TAG = "TestSuite";

	void log(bool result, String message) {

		if (result) {
			successLog += ("\n✓ " + message);
			successful++;

		} else {
			failureLog += ("\n✗ " + message);
			failures++;
		}

		Log::i(TAG, message);
	}

};

#endif /* FRAMEWORK_TESTSUITE */

