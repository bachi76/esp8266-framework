/*
 * ArrayList.h
 *
 * Simple fixed-size array helper. Not related to Java's ArrayList.
 *
 *  Created on: 27.11.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_FRAMEWORK_SRC_ARRAYLIST_H_
#define FRAMEWORK_FRAMEWORK_SRC_ARRAYLIST_H_

template <class TYPE, int ARRAY_LEN>
class ArrayList {

public:
	TYPE data[ARRAY_LEN];
	int size = 0;

	bool add(TYPE entry) {
		if (size == ARRAY_LEN) {
			Log::e(TAG, "ArrayList is full");
			return false;
		}

		data[size] = entry;
		size++;

		return true;
	}

	void print() {
		for (int i = 0; i < size; i++) {
			Serial.println(data[i]);
		}
	}

	int free() {
		return ARRAY_LEN - size;
	}

protected:
	const char* TAG = "ArrayList";

};



#endif /* FRAMEWORK_FRAMEWORK_SRC_ARRAYLIST_H_ */
