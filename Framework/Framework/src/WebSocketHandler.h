#ifndef WEB_SOCKET_HANDLER
#define WEB_SOCKET_HANDLER

#include <Log.h>
#include <WebSocketsServer.h>
#include <map>

/* Command function type */
typedef void (*FnPtr)(int, String);

/**
 * The WebSocketHandler enhances basic websockets and provides client authentication,
 * event handlers and send()/broadcast() helpers that send json-encoded ws messages.
 *
 * Usage: Create a subclass of WebSocketHandler and implement at least onVerifyLogin(), and override
 * the other event handlers as needed (typically at least onText() to handle incoming msgs)
 *
 * Security notes:
 * - ws:// (not wss://) is used, as tls-based connections seem not to be possible for now. Thus,
 *   sent data is unencrypted!
 * - onText() / onBin() events are only triggered for authenticated clients
 * - However, connected but unauthenticated clients still receive broadcast() messages!
 *   (see also https://github.com/Links2004/arduinoWebSockets/issues/200)
 *
 */
class WebSocketHandler {

public:

	const char* TAG = "WS";

	/* The underlying webSocket - for now hardcoded on port 81 */
	WebSocketsServer webSocket = WebSocketsServer(81);

	/* Successfully authenticated connections since start */
	int connectsSuccessful = 0;

	/* Failed connections (due to failed authentication) since start. */
	int connectsFailed = 0;


	/**
	 * Start the websockets server and handler
	 */
	void begin() {
		webSocket.onEvent([this](uint8_t num, WStype_t type, uint8_t * payload, size_t length){
			webSocketEvent(num, type, payload, length);
		});

		webSocket.begin();
	}

	/**
	 * Must be called every few milliseconds!
	 */
	void loop() {
		webSocket.loop();

		// Send heartbeats if needed - the rssi signal strength is sent too
		if (heartbeatPeriod > 0 && millis() > heartbeatLast + heartbeatPeriod) {
			webSocket.broadcastTXT("{\"rssi\":" + String(WiFi.RSSI()) + "}");
			//webSocket.broadcastPing();
			heartbeatLast = millis();
		}
	}

	/**
	 * Get the remote ip of the given client number
	 */
	String remoteIP(int num) {
		return webSocket.remoteIP(num).toString().c_str();
	}

	/**
	 * Send a broadcast to all connected clients.
	 *
	 * Attention: Broadcasts are also sent to connected, unauthenticated clients!
	 * (or we would have to override the webSocketServer lib)
	 */
	void broadcast(String key, String value) {
		String json = String("{\"") + key + String("\":") + jsonEncapsulateValue(value) + String("}");
		webSocket.broadcastTXT(json);
		Log::d(TAG, "Broadcasting: " + json);
	}

	/**
	 * Send a broadcast to all connected clients.
	 *
	 * Attention: Broadcasts are also sent to connected, unauthenticated clients!
	 * (or we would have to override the webSocketServer lib)
	 */
	void broadcast(String key, int value) {
		String json = String("{\"") + key + String("\":") + String(value) + String("}");
		webSocket.broadcastTXT(json);
		Log::d(TAG, "Broadcasting: " + json);
	}

	/**
	 * Send a broadcast to all connected clients.
	 *
	 * Attention: Broadcasts are also sent to connected, unauthenticated clients!
	 * (or we would have to override the webSocketServer lib)
	 */
	void broadcast(String key, float value, int decimals) {
		String json = String("{\"") + key + String("\":") + String(value, decimals) + String("}");
		webSocket.broadcastTXT(json);
		Log::d(TAG, "Broadcasting: " + json);
	}

	/**
	 * Send a message to a client
	 *
	 * Attention: Messages are also sent to connected, unauthenticated clients!
	 * (or we would have to override the webSocketServer lib)
	 */
	void send(int client, String key, String value) {
		String json = String("{\"") + key + String("\":") + jsonEncapsulateValue(value) + String("}");
		webSocket.sendTXT(client, json);
		Log::d(TAG, "Sending: " + json + " to client " + client);
	}

	/**
	 * Send a message to a client
	 *
	 * Attention: Messages are also sent to connected, unauthenticated clients!
	 * (or we would have to override the webSocketServer lib)
	 */
	void send(int client, String key, int value) {
		String json = String("{\"") + key + String("\":") + String(value) + String("}");
		webSocket.sendTXT(client, json);
		Log::d(TAG, "Sending: " + json + " to client " + client);
	}

	/**
	 * Send a message to a client
	 *
	 * Attention: Messages are also sent to connected, unauthenticated clients!
	 * (or we would have to override the webSocketServer lib)
	 */
	void send(int client, String key, float value, int decimals) {
		String json = String("{\"") + key + String("\":") + String(value, decimals) + String("}");
		webSocket.sendTXT(client, json);
		Log::d(TAG, "Sending: " + json + " to client " + client);
	}

	/**
	 * Add a command-to-function mapping. The mapped function will be called by the default onText() implementation.
	 *
	 * Example:
	 *
	 * Function mapping:	wsHandler.addCommand('aCommand', myCommand); // myCommand(String params)' must be defined
	 * WS text:			 	"aCommand:some-param-string-the:function_must handle"
	 * Calls: 				aCommand(clientId, "some-param-string-the:function_must handle");
	 */
	void addCommand(String command, FnPtr functionPointer) {
		commandMap[command] = functionPointer;
	}

	/**
	 * Set how often a hearbeat ("ping") should be sent to all connected clients. This can help clients to reconnect
	 * in case of stalled (but not closed) connections.
	 */
	void setHeartbeat(int ms) {
		heartbeatPeriod = ms;
		heartbeatLast = millis();
	}

	/**
	 * Broadcast a log entry as type "msg" to all connected ws clients (even unauthenticated ones).
	 * You can also use Log::<method> and enable remote logging instead.
	 */
	void log(const char* severity, const char* tag, const char* msg) {

		String sev = String(severity[0]);
		sev.toLowerCase();

		broadcast("msg", String(sev + String(":") + String(msg)));
		// FIXME: Use reserved memory and better concat
	}



protected:

	// Restrict the maximum of clients. 10 is more than the ESP possibly can handle.
	const static int MAX_CLIENTS = 10;

	// Keep ws client auth states: -1 = authenticated, 0..n = not auth, n'th try
	int clientAuthState[MAX_CLIENTS] = { 0 };

	// Maps ws-initiated commands to functions
	std::map<String, FnPtr> commandMap;

	// Hearbeat period in ms, 0 = off
	int heartbeatPeriod = 0;

	// Last hearbeat sent at
	long heartbeatLast = 0;


	/**
	 * Called on new connections, before any login.
	 *
	 * Return true if the client needs to authenticate, false otherwise.
	 * If fals is returned, the client is immediately marked as authenticated.
	 */
	virtual bool onConnect(uint8_t num, uint8_t * payload, size_t length) {
		return true;
	};

	/**
	 * Called on client disconnect.
	 */
	virtual void onDisconnect(uint8_t num, uint8_t * payload, size_t length) {

	};

	/**
	 * Verify the login credential(s) sent by the client.
	 * Return true if successful, false otherwise.
	 */
	virtual bool onVerifyLogin(uint8_t num, String incoming, size_t length) = 0;

	/**
	 * Called after a successful client login.
	 */
	virtual void onLoginSuccessful(uint8_t num) {
		send(num, "msg", "Login successful");
	};

	/**
	 * Called after a failed client login.
	 */
	virtual void onLoginFailed(uint8_t num, uint8_t * payload, size_t length) {

	};

	/**
	 * Called when ws text is received.
	 * Note: Only called if the client is successfully authenticated.
	 *
	 * The default implementation will split the command and params (':' delimiter),
	 * then call the function pointer that was previously assigned to this command (using addCommand()).
	 *
	 * Function mapping:	wsHandler.addCommand('aCommand', myCommand); // myCommand(String params)' must be defined
	 * WS text:			 	"aCommand:some-param-string-the:function_must handle"
	 * Calls: 				aCommand(clientId, "some-param-string-the:function_must handle");
	 *
	 * Overwrite to implement your own ws text handling.
	 */
	virtual void onText(uint8_t num, String incoming, size_t length) {

		String cmd;
		String params;

		int delimPos = incoming.indexOf(':');
		if (delimPos >= 1) {
			cmd = incoming.substring(0, delimPos);
			params = incoming.substring(delimPos + 1);

		} else {
			cmd = incoming;
			params = "";
		}

		if (commandMap.count(cmd) > 0) {
			commandMap[cmd](num, params);

		} else {

			Log::e(TAG, "Unsupported command: " + cmd);
			send(num, "msg", "e:Unsupported command: " + cmd);
		}
	};


	/**
	 * Called when receiving a new ws message (as binary data).
	 *
	 * Note: Only called if the client is successfully authenticated.
	 */
	virtual void onBin(uint8_t num, uint8_t * payload, size_t length) {

	};


	void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

		String incoming = (char*)payload;

		switch(type) {

		case WStype_DISCONNECTED:

			if (num < MAX_CLIENTS) {
				clientAuthState[num] = 0;
			}
			Log::d(TAG, String(num) + " disconnected!");
			onDisconnect(num, payload, length);

			break;

		case WStype_CONNECTED: {

			bool loginRequired = onConnect(num, payload, length);

			// This should not be possible as the client number is limited for the ESP,
			// but let's make sure.
			if (num > MAX_CLIENTS - 1) {
				send(num, "msg", "Maximum clients reached, aborting");
				webSocket.disconnect(num);
				return;
			}

			if (loginRequired) {
				// Login first
				clientAuthState[num] = 0;
				send(num, "msg", "Please log in.");
				send(num, "login", clientAuthState[num]);

			} else {
				continueAfterLogin(num);
			}

		}
		break;

		case WStype_TEXT:
		case WStype_BIN:

			if (clientAuthState[num] != -1) {

				if (onVerifyLogin(num, incoming, length)) {
					continueAfterLogin(num);
					return;

				} else {
					updateConnects(false);
					Log::d(TAG, "[" + String(num) + "] login failed.");
					send(num, "msg", "Login failed!");
					clientAuthState[num]++;
					send(num, "msg", "Please log in.");
					send(num, "login", clientAuthState[num]);

					onLoginFailed(num, payload, length);
					return;
				}
			}

			// From here we're authenticated

			if (type == WStype_TEXT) {
				Log::d(TAG, "[" + String(num) + "] incoming: " + incoming);
				onText(num, incoming, length);
			}

			if (type == WStype_BIN) {
				Log::d(TAG, "[" + String(num) + "] incoming binary data");
				onBin(num, payload, length);
			}

			break;
		}
	};

	/**
	 * After successful login, send config data etc.
	 */
	void continueAfterLogin(int num) {

		clientAuthState[num] = -1;
		updateConnects(true);
		Log::d(TAG, "[" + String(num) + "] User logged in.");
		yield();

		send(num, "login", -1);
		onLoginSuccessful(num);
	};

	void updateConnects(bool successful) {
		if (successful) {
			broadcast("connectsSuccessful", ++connectsSuccessful);
		} else {
			broadcast("connectsFailed", ++connectsFailed);
		}

	};

	virtual ~WebSocketHandler(){}


private:

	String jsonEncapsulateValue(String value) {
		if ((value.startsWith("[") && value.endsWith("]"))
				|| (value.startsWith("{") && value.endsWith("}"))) {
			return value;
		} else {
			return String("\"") + value + String("\"");
		}
	}
};

#endif
