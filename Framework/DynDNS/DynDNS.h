/*
 * DynDNS.h
 *
 *  Created on: 08.12.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_DYNDNS_DYNDNS_H_
#define FRAMEWORK_DYNDNS_DYNDNS_H_

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <Log.h>

/**
 * Update a DynDNS (dyn.com) host entry with a new IP (either a provided internal one for access in the same
 * network only, or then let DynDNS take the originating, public ip (which in most cases requires
 * you to provide a host mapping in your router to allow public access).
 *
 * Note: Be careful not to spam Dyn with too many update requests or they might block your host.
 *
 * Note: For internal networks, it's probably better to use .local addresses and the built-in mDNS responder
 * if your network supports it.
 */
class DynDNS {

public:

	static const char* TAG;

	/**
	 * The DynDNS return code of the last update request. See  https://help.dyn.com/remote-access-api/return-codes/
	 */
	static String lastReturnCode;

	static const char* updateHost;
	static const uint16_t port = 443;

	/**
	 * Update a DynDNS host. Let DynDNS guess the IP. This means the public, web-facing IP,
	 * i.e. of your router, will be used. You'll need to add a port mapping to your router to
	 * allow public access to your esp.
	 */
	static boolean update(String host, String base64AuthString) {
		return update(host, "", base64AuthString);
	}

	/**
	 * Update a DynDNS host and provide the IP. If you use your internal ip, you'll be able to
	 * access your esp with this host name in the same network only.
	 */
	static boolean update(String host, String ip, String base64AuthString) {

		// Update hostname via GET request (see https://help.dyn.com/remote-access-api/perform-update/ )
		String request = "GET /nic/update?hostname=" + host + (!ip.equals("") ? "&myip=" + ip : "") + "&wildcard=NOCHG&mx=NOCHG&backmx=NOCHG HTTP/1.0\r\n" +
						"Host: " + updateHost + "\r\n" +
						"Authorization: Basic " + base64AuthString + "\r\n" +
						"User-Agent: ESP8266\r\n" +
						// "Connection: close\r\n" +
						"\r\n";

		if (executeRequest(request)) {
			Log::i(TAG, "DynDNS update for " + host + " successful. Return code: " + lastReturnCode);
			return true;

		} else {

			Log::e(TAG, "Could not update DynDNS host " + host + ". Error code: " + lastReturnCode);
			return false;
		}
	}


private:

	//const char* _fingerprint = "...";

	static bool executeRequest(String request) {

		WiFiClientSecure client = WiFiClientSecure();

		if (!connect(client)) {
			return false;
		}

		Log::d(TAG, "Memory left after connect: " + String(ESP.getFreeHeap()));

		yield();

		Log::d(TAG, "Sending request:\n" + request);
		client.print(request);

		while(client.connected() && !client.available()) {
			delay (50);
			Serial.print(".");
		}

		lastReturnCode = getBody(client.readString());

		// Handle response (as defined in https://help.dyn.com/remote-access-api/return-codes/ )
		if (lastReturnCode.startsWith("good")) {
			return true;

		} else if (lastReturnCode.startsWith("nochg")) {

			// The update changed no settings, and is considered abusive.
			// Additional nochg updates will cause the hostname to become blocked.
			Log::w(TAG, "DynDNS returned 'nochg' - request didn't change anything and is thus considered abusive!");
			return true;

		} else {
			return false;
		}
	}

	static bool connect(WiFiClientSecure &_client) {
		if (!_client.connect(updateHost, port)) {
			Log::e(TAG, "Connection failed");
			return false;
		}
//		if (!_client.verify(_fingerprint, _host)) {
//			Log::e(TAG, "Certificate doesn't match");
//			return false;
//		}
		if (!_client.connected()) {
			Log::d(TAG, "Not connected");
			return false;
		}

		Log::d(TAG, "Connected");
		return true;
	}

	static String getBody(String response) {
		unsigned int pos = response.indexOf("\r\n\r\n");
		if (pos < 1) return "";
		if (response.length() < pos + 4) return "";
		return response.substring(pos + 4);
	}

};

String DynDNS::lastReturnCode = "";
const char* DynDNS::TAG = "DynDNS";
const char* DynDNS::updateHost = "members.dyndns.org";


#endif /* FRAMEWORK_DYNDNS_DYNDNS_H_ */
