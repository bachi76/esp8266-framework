/*
 * Gain access to the Firebase cloud-based real-time db.
 * A library for the ESP6288. Uses the Firebase REST API
 * (https://www.firebase.com/docs/rest/api/)
 *
 * Note: The TLS requests take about 22kb of the about 36kb available
 * ram on the esp8266, keep this in mind.
 *
 * Status: Beta
 *
 *  Created on: 18.11.2015
 *      Author: bachi
 */

#ifndef LIBRARIES_FIREBASE_SRC_FIREBASE_H_
#define LIBRARIES_FIREBASE_SRC_FIREBASE_H_

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

#include <Log.h>


typedef struct Response {
	/* True if the Firebase server returned with a 200 OK */
	bool success;

	/* True if the operation returned the null response */
	bool null;

	/* First line of the response header, containing the response code */
	String header;

	/* Body of the response */
	String data;
};

class Firebase {

public:

	/**
	 * Access a public firebase db.
	 * Path defines the entry point, use "/" for accessing the root element.
	 */
	Firebase(char* host, String path) {
		if (!path.startsWith("/")) path = "/" + path;
		if (!path.endsWith("/")) path = path + "/";

		_host = host;
		_path = path;
		_authToken = ""; // TODO
	}

	/**
	 * Access a private firebase db using the authToken
	 * Path defines the entry point, use "/" for accessing the root element.
	 * TODO: -- not implemented yet --
	 */
	Firebase(char* host, String path, char* authToken) {
		if (!path.startsWith("/")) path = "/" + path;
		if (!path.endsWith("/")) path = path + "/";

		_host = host;
		_path = path;
		_authToken = authToken;
	}

	/**
	 * Get a json formatted string for a key/value pair
	 */
	String keyVal(String key, String value) {
		return "{\"" + key + "\":\"" + value + "\"}";
	}

	/**
	 * Get the node at the specified location as json.
	 * If not found, response.success is false.
	 */
	Response getNode(String location) {
		String request = buildRequest("GET", location, "");
		Response response =  executeRequest(request);

		// Firebase returns a 200 for missing keys - we interpret that as failed
		if (response.success && response.null) response.success = false;

		return response;
	}

	/**
	 * Set a node using a json string.
	 */
	Response setNode(String location, String json)  {
		String request = buildRequest("PATCH", location, json);
		return executeRequest(request);
	}

	/**
	 * Get a direct value (no json) for a key/value pair.
	 */
	Response getValue(String key) {
		String request = buildRequest("GET", key, "");
		Response response =  executeRequest(request);

		// Firebase returns a 200 for missing keys - we interpret that as failed
		if (response.success && response.null) {
			response.success = false;
			return response;
		}

		// Pure value responses are in "", so cut them if found
		if (response.data.startsWith("\"") && response.data.endsWith("\"")) {
			response.data = response.data.substring(1, response.data.length() - 1);
		}

		return response;
	}

	/**
	 * Directly set a value (no json).
	 */
	Response setValue(String key, String value)  {

		String path = "/";

		// Cut a combined path+key into path and key
		int pos = key.lastIndexOf("/");
		if (pos > -1 && !key.endsWith("/")) {

			path = key.substring(0, pos);
			key = key.substring(pos+1);

			if (!path.startsWith("/")) path = "/" + path;
		}

		String request = buildRequest("PATCH", path, "\"" + key + "\":\"" + value + "\"");
		return executeRequest(request);
	}


	/**
	 * Add a new entry/row to a json array
	 * (uses the REST APIs POST request)
	 */
	Response addRow(String location, String json)  {
		String request = buildRequest("POST", location, json);
		return executeRequest(request);
	}

	/**
	 * Remove a node.
	 */
	Response remove(String location) {

		//  Prevent accidential db deletes - use explicit dropDB() instead
		if (location == "" || location == "/") {
			Response err;
			err.header = "Err: Cannot remove root";
			err.success = false;
			Log::e(TAG, err.header);
			return err;
		}
		String request = buildRequest("DELETE", location, "");
		return executeRequest(request);
	}


	/**
	 * Drop the db (delete starting from the path defined upon instantiation)
	 */
	Response dropDB() {
		String request = buildRequest("DELETE", _path, "");
		return executeRequest(request);
	}


private:

	const char* TAG = "Firebase";
	const char* _fingerprint = "C1 56 CD D8 49 A3 7D D2 1D 49 60 7E 0D 59 A7 7C C1 0E 58 D2";
	const uint16_t _port = 443;

	char* _host;
	char* _authToken;
	String _path;

	bool connect(WiFiClientSecure &_client) {
		if (!_client.connect(_host, _port)) {
			Log::e(TAG, "Connection failed");
			return false;
		}
//		if (!_client.verify(_fingerprint, _host)) {
//			Log::e(TAG, "Certificate doesn't match");
//			return false;
//		}
		if (!_client.connected()) {
			Log::d(TAG, "Not connected");
			return false;
		}

		Log::d(TAG, "Connected");
		return true;
	}

	String buildRequest(String verb, String location, String payload) {

		// Sanitize the location
		location = _path + location;
		if (!location.endsWith(".json")) location += ".json";

		if (payload.length() > 0 && !payload.startsWith("{")) {
			payload = "{" + payload + "}";
		}

		String payloadLine = payload.length() > 0 ? ("Content-Length: " + String(payload.length()) + String("\r\n\r\n") + payload) : "";

		String request = verb + " " + location + /*+ (_authToken != "" ? "?auth=" + String(_authToken) : "")*/  + " HTTP/1.1\r\n" +
				"Host: " + _host + "\r\n" +
				"User-Agent: ESP8266\r\n" +
				"Connection: close\r\n" +
				payloadLine +
				"\r\n";

		return request;
	}


	Response executeRequest(String request) {

		Log::d(TAG, "Memory left before Wifi client: " + String(ESP.getFreeHeap()));

		WiFiClientSecure client = WiFiClientSecure();
		Response response;

		Log::d(TAG, "Memory left before connect: " + String(ESP.getFreeHeap()));

		if (!connect(client)) {
			response.success = false;
			response.header = "Cannot connect to remote host";
			return response;
		}

		Log::d(TAG, "Memory before sending request: " + String(ESP.getFreeHeap()));

		Log::d(TAG, "Sending request:\n" + request);
		client.print(request);

		while(client.connected() && !client.available()) {
			delay (50);
			Serial.print("-");
		}

		response.header = client.readStringUntil('\n');

		response.data = getBody(client.readString());
		Log::d(TAG, "Response header: " + response.header);
		Log::d(TAG, "Response data:" + response.data);

		response.null = response.data.equals("null");

		if (response.header.indexOf("200 OK") > 0) {
			response.success = true;

		} else {
			response.success = false;

			Log::e(TAG, "Error. Response headers:" + response.header);

			while (client.connected()) {
				String data = client.readStringUntil('\n');
				Serial.println("< " + data);
			}

			Serial.println("Response data:");
			while(client.available() > 0) {
				String data = client.readStringUntil('\n');
				Serial.println("< " + data);
			}
		}

		Log::d(TAG, "Memory left after request: " + String(ESP.getFreeHeap()));

		return response;
	}


	String unquote(String s)  {
		if (s[0] == '"' && s[s.length()-1] == '"') {
			return s.substring(1, s.length()-1);
		}
		return s;
	}


	String getBody(String response) {
		unsigned int pos = response.indexOf("\r\n\r\n");
		if (pos < 1) return "";
		if (response.length() < pos + 4) return "";
		return response.substring(pos + 4);
	}

};

#endif /* LIBRARIES_FIREBASE_SRC_FIREBASE_H_ */
