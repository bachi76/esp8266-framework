/*
 * WC_Routes.h
 *
 * /api/directory: Outputs a list of all configured webserver routes
 *
 *  Created on: 26.11.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_WEBCOMPONENTS_SRC_WC_ROUTES_H_
#define FRAMEWORK_WEBCOMPONENTS_SRC_WC_ROUTES_H_

#include "ESP8266WebServerInspectable.h"

class WC_Routes {

public:

	WC_Routes() {
		WC_Routes("/api/directory");
	}

	WC_Routes(const char* path){

		ESP8266WebServerInspectable::instance->on(path, [](){
			WC_Routes::handleDirectory();
		});
	}

	static void handleDirectory() {

		ESP8266WebServerInspectable::instance->routes.print();

		String response;
		for (int i=0; i < ESP8266WebServerInspectable::instance->routes.size; i++) {
			response = response + ESP8266WebServerInspectable::instance->routes.data[i] + String("\n");
		}

		ESP8266WebServerInspectable::instance->send(200, "text/plain", response);
	}


};

#endif /* FRAMEWORK_WEBCOMPONENTS_SRC_WC_ROUTES_H_ */
