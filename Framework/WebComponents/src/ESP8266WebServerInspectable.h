/*
 * ESP8266WebServerInspectable.h
 *
 * Subclass of ESP8266WebServer which keeps track of (up to 20) configured routes.
 * (as currently there's no way of accessing them directly)
 *
 * Used for listing all configured routes.
 *
 *  Created on: 20.11.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_WEBCOMPONENTS_ESP8266WebServerInspectable
#define FRAMEWORK_WEBCOMPONENTS_ESP8266WebServerInspectable

#include <ESP8266WebServer.h>
#include <Log.h>
#include <ArrayList.h>

class ESP8266WebServerInspectable : public ESP8266WebServer {

public:

	ArrayList<String, 20> routes;
	static ESP8266WebServerInspectable *instance; // FIXME: replace with instance() and check / error msg if we have none..

	ESP8266WebServerInspectable(IPAddress addr, int port) : ESP8266WebServer(addr, port) {
		instance = this;
	}
	ESP8266WebServerInspectable(int port) : ESP8266WebServer(port) {
		instance = this;
	}

	void on(const char* uri, ESP8266WebServer::THandlerFunction handler) {
		ESP8266WebServer::on(uri, handler);
		routes.add(uri);
	}

	void on(const char* uri, HTTPMethod method, ESP8266WebServer::THandlerFunction fn) {
		ESP8266WebServer::on(uri, method, fn, _fileUploadHandler);
		routes.add(uri);
	}

	void on(const char* uri, HTTPMethod method, ESP8266WebServer::THandlerFunction fn, ESP8266WebServer::THandlerFunction ufn) {
		ESP8266WebServer::on(uri, method, fn, ufn);
		routes.add(uri);
	}

	void handleClient() {
		ESP8266WebServer::handleClient();
	}

};

ESP8266WebServerInspectable *ESP8266WebServerInspectable::instance;


#endif // FRAMEWORK_WEBCOMPONENTS_ESP8266WebServerInspectable
