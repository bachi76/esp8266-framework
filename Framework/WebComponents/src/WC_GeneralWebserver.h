/*
 * WC_GeneralWebserver.h
 *
 * Serves static files from the spiffs file system.
 *
 *  Created on: 01.12.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_WEBCOMPONENTS_SRC_WC_GENERALWEBSERVER_H_
#define FRAMEWORK_WEBCOMPONENTS_SRC_WC_GENERALWEBSERVER_H_

#include "ESP8266WebServerInspectable.h"
#include <FS.h>
#include <Log.h>

class WC_GeneralWebserver {

public:

	/**
	 * staticFileVersion should be increased whenever the static files change, to ensure browsers
	 * receive the new versions (used in etag headers). Could be read from a Config value
	 * (which then can be changed in /config.json when working with spiffs files).
	 */
	int staticFileVersion = 1;

	/**
	 * Show the file list if a path ends with / and the folder does not contain an index.html?
	 */
	bool allowDirBrowsing = true;


	/**
	 * Initialize the static spiffs files webserver.
	 */
	WC_GeneralWebserver(String wwwRoot) {

		if (wwwRoot.length() > 1 && (!wwwRoot.startsWith("/") || wwwRoot.endsWith("/"))) {
			Log::e(TAG, "Config error: wwwRoot must start with / and not end with /");
		}

		if (wwwRoot.equals("/")) {
			wwwRoot = "";
		}

		_wwwRoot =  wwwRoot;
	}

	/**
	 * Start the general webserver
	 */
	void begin() {

		// Init the FS
		if (!SPIFFS.begin()) {
			Log::e(TAG, "Could not mount FS");
		}

		// Collect request headers
		const char* headers[] = {"If-None-Match"}; // etag
		ESP8266WebServerInspectable::instance->collectHeaders(headers, 1);

		//called when the url is not defined here
		//use it to load content from SPIFFS
		ESP8266WebServerInspectable::instance->onNotFound([this](){

			if(!handleFileRead(ESP8266WebServerInspectable::instance->uri())) {

				if (ESP8266WebServerInspectable::instance->uri().endsWith(".html") && SPIFFS.exists(_wwwRoot + "/404.html")) {
					File file = SPIFFS.open(_wwwRoot + "/404.html", "r");
					ESP8266WebServerInspectable::instance->streamFile(file, "text/html");
					Log::d(TAG, "Sent 404 file"); // todo - send 404 header
					file.close();
					return;

				} else {
					ESP8266WebServerInspectable::instance->send(404, "text/plain", "File not found.");
				}
			}
		});

	}


protected:

	String _wwwRoot;
	const char* TAG = "GenWS";

	bool handleFileRead(String uri){

		// build the path, prevent double-/
		String path = _wwwRoot + uri;

		Log::d(TAG, "Request for static file: " + path);

		String etag = getETag(path);

		// If the etags match, return a 304 not modified
		String ifNoneMatch = ESP8266WebServerInspectable::instance->header("If-None-Match");
		if (ifNoneMatch != "") Log::d(TAG, "Comparing: '" + etag + "' with '" + ifNoneMatch + "'");

		// Etag handling
		if (etag.equals(ifNoneMatch)) {
			Log::d(TAG, "Client has matching etag - returning 304");
			ESP8266WebServerInspectable::instance->sendHeader("Etag", etag);
			ESP8266WebServerInspectable::instance->send(304, ""); // TODO: How to send only a header?
			return true;
		}

		String origPath = path;
		if(path.endsWith("/")) path += "index.html";
		String contentType = getContentType(path);
		String pathWithGz = path + ".gz";

		File file;
		boolean found = false;
		if(SPIFFS.exists(pathWithGz)) {
			path = pathWithGz;
			found = true;

		} else if (SPIFFS.exists(path)){
			found = true;
		}

		if (found) {
			File file = SPIFFS.open(path, "r");
			Log::d(TAG, "Sending " + path);
			ESP8266WebServerInspectable::instance->sendHeader("Etag", etag);
			ESP8266WebServerInspectable::instance->streamFile(file, contentType);
			file.close();
			return true;

		} else if (origPath.endsWith("/") && allowDirBrowsing) {
			handleFileList(origPath);
			return true;

		} else if (pathWithGz.length() > 32) {
			Log::w(TAG, "Path too long for SPIFFS (max 32 char): " + pathWithGz);
		}

		return false;

	}

	void handleFileList(String path) {
		Log::d(TAG, "Showing file list of " + path);
		Dir dir = SPIFFS.openDir(path);
		path = String();

		String output = "[";
		while(dir.next()){
			File entry = dir.openFile("r");
			if (!entry) break;
			if (output != "[") output += ',';
			bool isDir = false;
			output += "{\"type\":\"";
			output += (isDir)?"dir":"file";
			output += "\",\"name\":\"";
			output += String(entry.name()).substring(1);
			output += "\"}";
			entry.close();
		}

		output += "]";
		ESP8266WebServerInspectable::instance->send(200, "text/json", output);
	}

	String getETag(String path) {
		return path + "-v" + String(staticFileVersion); // could also read a version file from the fs here
	}

	String getContentType(String filename){
		if(ESP8266WebServerInspectable::instance->hasArg("download")) return "application/octet-stream";
		else if(filename.endsWith(".htm")) return "text/html";
		else if(filename.endsWith(".html")) return "text/html";
		else if(filename.endsWith(".css")) return "text/css";
		else if(filename.endsWith(".js")) return "application/javascript";
		else if(filename.endsWith(".png")) return "image/png";
		else if(filename.endsWith(".gif")) return "image/gif";
		else if(filename.endsWith(".jpg")) return "image/jpeg";
		else if(filename.endsWith(".ico")) return "image/x-icon";
		else if(filename.endsWith(".xml")) return "text/xml";
		else if(filename.endsWith(".pdf")) return "application/x-pdf";
		else if(filename.endsWith(".zip")) return "application/x-zip";
		else if(filename.endsWith(".gz")) return "application/x-gzip";
		return "text/plain";
	}
};

#endif /* FRAMEWORK_WEBCOMPONENTS_SRC_WC_GENERALWEBSERVER_H_ */
