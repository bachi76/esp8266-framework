/*
 * WC_RemotePowerSwitch.h
 *
 * Adds routes to control radio-controlled power switches:
 *
 * /api/switch/<name>/on - turn switch on
 * /api/switch/<name>/off - turn switch off
 *
 *  Created on: 26.11.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_WEBCOMPONENTS_SRC_WC_REMOTEPOWERSWITCH_H_
#define FRAMEWORK_WEBCOMPONENTS_SRC_WC_REMOTEPOWERSWITCH_H_

#include "ESP8266WebServerInspectable.h"
#include <RCSwitch.h>
#include <Log.h>

class WC_RemotePowerSwitch {

public:

	RCSwitch rcSwitch;

	WC_RemotePowerSwitch(int transmitPin, const char* path) {
		_path = path;
		rcSwitch.enableTransmit(transmitPin);
	}
	WC_RemotePowerSwitch(int transmitPin) {
		_path = "/api/switch";
		rcSwitch.enableTransmit(transmitPin);
	}

	void addSwitch(const char* name, const int group, const int device) {

		Log::i(TAG, "Adding switch to api: " + String(name));

		String on = String(_path) + String("/") + String(name) + "/on";
		ESP8266WebServerInspectable::instance->on(on.c_str(), [this, group, device](){
			rcSwitch.switchOn(group, device);
			ESP8266WebServerInspectable::instance->send(200, "text/plain", "OK");
		});

		String off = String(_path) + String("/") + String(name) + "/off";
		ESP8266WebServerInspectable::instance->on(off.c_str(), [this, group, device](){
			rcSwitch.switchOff(group, device);
			ESP8266WebServerInspectable::instance->send(200, "text/plain", "OK");
		});

	}

protected:
	const char* _path;
	const char* TAG = "WC_RemotePowerSwitch";

};

#endif /* FRAMEWORK_WEBCOMPONENTS_SRC_WC_REMOTEPOWERSWITCH_H_ */
