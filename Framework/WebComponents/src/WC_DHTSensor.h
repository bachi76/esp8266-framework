/*
 * WC_DHTSensor.h
 *
 * Adds routes to control radio-controlled power switches:
 *
 * /api/switch/<name>/on - turn switch on
 * /api/switch/<name>/off - turn switch off
 *
 *  Created on: 26.11.2015
 *      Author: bachi
 */

#ifndef FRAMEWORK_WEBCOMPONENTS_SRC_WC_DHTSENSOR_H_
#define FRAMEWORK_WEBCOMPONENTS_SRC_WC_DHTSENSOR_H_

#include "ESP8266WebServerInspectable.h"
#include <Log.h>
#include <DHT.h>


class WC_DHTSensor {

public:

	WC_DHTSensor()


//		String off = String(_path) + String("/") + String(name) + "/off";
//		ESP8266WebServerInspectable::instance->on(off.c_str(), [this, group, device](){
//			rcSwitch.switchOff(group, device);
//			ESP8266WebServerInspectable::instance->send(200, "text/plain", "OK");
//		});



protected:

	const char* TAG = "WC_DHT";

};

#endif /* FRAMEWORK_WEBCOMPONENTS_SRC_WC_DHTSENSOR_H_ */
