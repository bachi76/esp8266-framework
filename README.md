#ESP 8266 Framework#

This project provides several libraries for Arduino on the ESP8266. It is based on the [Arduino core for ESP8266](https://github.com/esp8266/Arduino).

The following libraries are available:

## Framework ##

Contains some base libs (Config.h, Log.h, Test.h etc). All other libs here require the framework lib.

### Config.h ###

Configuration base class to handle persistent config values. Loads from and stores to flash memory using the SPIFFS filesystem.

Data is stored in Json format. Multiple config files are supported. To use, subclass it and overwrite onLoad() / onSave() to extract / add config values from/to the passed json object.

Usage:


```
#!c++
	// Create a subclass and add your config values.
	class MyConfig : public Config {

	public:

		// Define defaults
		char* keyChar = "initial";
		int keyInt = 7;
		String keyString = "initial";

		MyConfig() : Config() {};
		MyConfig(char* configPath) : Config(configPath) {}


		// Load config values
		void onLoad(JsonObject &json) {

			if (json.containsKey("keyChar")) keyChar =  strcpy(keyChar, json["keyChar"]);
			if (json.containsKey("keyInt")) keyInt = json["keyInt"];
			if (json.containsKey("keyString")) keyString = String(json["keyString"].asString());
		}

		// Save r/w values
		void onSave(JsonObject &json) {

			json["keyChar"] = keyChar;
			json["keyInt"] = keyInt;
			json["keyString"] = keyString;

		}
	};

    // You can use multiple config files.
    MyConfig cfg = MyConfig("config.json");
    cfg.load();
    Serial.println(cfg.keyString);
    ... 
    
    cfg.keyString = "updated";
    cfg.save();

```

### Util ###

Static helper methods.

```
#!c++
// Run in setup(), once
Util::connectToWiFi(ssid, password, mode);

// Run in loop(), tries to reconnect if wifi connection was lost
Util::keepConnected(ssid, password, mode)
```

### Access a remote IoT Server instance ###
The [IotServer](https://bitbucket.org/bachi76/iot-server/) puts a small REST API layer suitable for iot devices in front of the mighty **ELK stack** ([ElasticSearch](https://www.elastic.co/products/elasticsearch) and [Kibana](https://www.elastic.co/products/kibana)), **packed neatly in a Docker image**. Log and visualize your data in no time! For more info see [IotServer](https://bitbucket.org/bachi76/iot-server/).

```
#!c++
// Create the iot instance
IotServer iot("<iot api url>", "<app token>");

// Store a single key/value pair
iot.post("adc", analogRead(A0));

// Create and post a custom record (document in Elastic-speak)
StaticJsonBuffer<200> buf;
JsonObject& json = buf.createObject();

json["adc"] = analogRead(A0);
json["free_heap"] = (int)ESP.getFreeHeap();
json["msg"] = "Pump turned on!";

iot.post("event", json);
```

### Log ###

Logging (to Serial and remote [IotServer](https://bitbucket.org/bachi76/iot-server/)) in a standardized way - using caller TAGs and severity levels. Android Log-like API.

```
// Log an error
Log::e("MyTag", "Some error happened");

#!c++
// To log info, warnings and errors to the remote IotServer
Log::enableRemoteLogging(iot);

// Disable all log output when not needed
Log::enabled = false;
```

### Test ###

Some static helper methods useful for writing tests.

```
#!c++
    TestSuite t = TestSuite("DynDNS");
    t.test("First test", testThatShouldReturnTrue());
    t.testString("Second test", testReturningString(), "Expected Result");
    ...
    t.printReport();
```

## DynDNS ##
Update a DynDNS (dyn.com) host entry with a new IP (either a provided internal one for access in the same network only, or then let DynDNS take the originating, public ip (which in most cases requires you to provide a host mapping in your router to allow public access).

See https://help.dyn.com/remote-access-api/perform-update/

Be careful not to spam Dyn with too many update requests or they might block your host.

Note: For internal networks, it's probably better to use .local addresses and the built-in mDNS responder if your network supports it.

```
#!c++
#include <DynDNS.h>

DynDNS::update(host, ip, base64AuthString);
```
Create your base64 encoded auth string from "username:passwordOrBetterAuthToken" - you can use an online base64 encoder like http://www.freeformatter.com/base64-encoder.html

## Firebase ##

Gain access to the Firebase cloud-based real-time database. Allows to read, write and update key/values or lists. Uses the Firebase REST API (https://www.firebase.com/docs/rest/api/)

Requires the [ArduinoJson](https://github.com/bblanchon/ArduinoJson) library.


```
#!c++
	#include <ArduinoJson.h>
	#include <Firebase.h>

	// Use a root path to confine all operations to that path (and not the whole db)
	Firebase fb("<mydb>.firebaseio.com", "rootPathElement");

	// To create simple key/value json
	String keyVal = fb.keyVal("putkey1", "putvalue1");
	
	// Add a row to a the node 'key'
	fb.addRow("key", keyVal);

	// Set a node
	fb.setNode("data", keyVal);

	// Remove a node
	fb.remove("data");

	// Add a single key/value	
	fb.setValue("key", "value");
	String val = fb.getValue("key").data
	
	// Keys can contain paths
	fb.setValue("node1/node2/subkey", "value");
	
	// It's a good idea to test each operation for success
	if (!fb.setValue("key", "val").success) {
		// do something!
	}
	
	// Drop everything
	fb.dropDB();
```

Note: The https/tls requests take about 22kb of the about 36kb available ram on the arduino core esp8266, keep this in mind.


## WebComponents ##

Various components that add standard webserver routes, with the aim to quickly build a unified rest api.

Note: All WebComponents require the used webserver to be ESP8266WebServerInspectable, which is a subclass of ESP8266WebServer.

### WC_GeneralWebserver.h ###
Serves static web files from [SPIFFS](https://github.com/esp8266/Arduino/blob/master/doc/filesystem.md) filesystem - based on the arduino esp core example. 
Features:

* Serve html, css, js, images etc from spiffs
* www root folder to prevent access to non-public files such as config files
* Etag support for optimal browser caching (using a manual version counter)
* Optional directory browsing (open host.com/somepath/ with a trailing slash)
* Serve gzip'ed files

Important: The max path length (full path + filename) for spiffs is 32 chars! This is not enough for most standard web templates (e.g. Bootstrap), so you need to adjust them.


```
#!c++

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266WebServerInspectable.h>
#include <WC_GeneralWebserver.h>

// Define the underlying webserver. 
// Note: Need to use ESP8266WebServerInspectable for all web components.
ESP8266WebServerInspectable server(80);

// Define the static file webserver component and its web root folder
WC_GeneralWebserver wcGenWebserver("/www");

void setup() {

	Serial.begin(115200);
	Util::connectToWifi(wifi_ssid(), wifi_pw(), WIFI_STA);

	// Start the underlying webserver
	server.begin();

	// Allow directory browsing (default = true)
	wcGenWebserver.allowDirBrowsing = true;
	
	// Used in Etags. Increase this when static web files change.
	wcGenWebserver.staticFileVersion = 1;

	// Start the static file server (just adds routes to the underlying server)
	wcGenWebserver.begin();
	
void loop() {
	server.handleClient();
}
```

### WC_RemotePowerSwitch.h ###

Add radio-controlled power switches using the RCSwitch lib. Adds on/off routes for all added switches.

```
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266WebServerInspectable.h>
#include <WC_RemotePowerSwitch.h>

ESP8266WebServerInspectable server(80);

// Define the rcSwitch with data pin D7
WC_RemotePowerSwitch wcSwitch(D7);

void setup() {

    // Add webserver routes to turn on/off some radio-controlled power switches    
    wcSwitch.addSwitch("light", 1, 1);
    wcSwitch.addSwitch("heater", 1, 2);
}

void loop() {
    server.handleClient();
}
```
The above example adds these routes:

```
http://yourhost/api/switch/light/on
http://yourhost/api/switch/light/off
http://yourhost/api/switch/heater/on
http://yourhost/api/switch/heater/off
```


### WC_DHTSensor.h ###

[TODO] Adds temperature- and humidity readings from a DHT11 or DHT22

### WC_Routes.h ###

Creates a route that lists all generated webserver routes. Good for debugging, api docs or dynamic web frontends.

```
#!c++

ESP8266WebServerInspectable server(80);

// The routes list becomes available under /api/directory (default)
WC_Routes wcRoutes("/api/directory");

void setup() {
    ....
    
    // We can also output the configured routes via Serial
    Log::i(TAG, "Configured routes:");
    ESP8266WebServerInspectable::instance->routes.print();
}
```

## Contributions welcome ##

Missing a feature or found a bug? Feedback and pull requests are most welcome. I'm especially looking for a *Python server implementation* using the Autobahn python library.


## Legal note ##

Copyright 2016 Martin Bachmann, [insign gmbh](http://www.insign.ch)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this library except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.