ESP Framework - Tasks
=====================

### Test.h ###

[x] testInt()
[] testRange()


### Log.h ###

[] Level handling
[] remote logging (Firebase? Email? Url?)


### Firebase.h ###

[x] Fix remove
[x] putValue should handle /path/to/key
[x] Add a namespace param to constructor (appended to all paths) - used with deviceId


# Needed libraries #

### Configuration ###

[x] fail early?
[x] default values
[x] local storage
[] can update from remote
[] get a unique deviceId


### Setup ###

[] Start-up AP config wifi
[] Store wifi creds (and maybe other config values)
[] Try to connect to wifi and send feedback via AP? Via mixed mode conn?
[] Re-start config AP if not connected to wifi
[] Manually re-enable config AP via buttons?
[] Default config pw, configurable
[] Allow multiple wifi configs - try all 
[] Configurable debug settings (enable remote logging) - su?

### Status page ###

[x] Show all available pins (in and out)
[x] Can toggle available pins
[x] Show ADC mode and value
[x] Show program name and version




