The data folder is converted into a data.bin file containing the spiffs fs.
To create the bin file and upload it, use the Arduino IDE
-> "Tools > ESP6288 Sketch Data Upload"

Note: Disconnect any serial monitor in Eclipse while uploading!

=> You need to have the ESP8266FS tool installed. See
https://github.com/esp8266/Arduino/blob/master/doc/filesystem.md#uploading-files-to-file-system

