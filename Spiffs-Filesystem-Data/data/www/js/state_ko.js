
var host = '';
var espState = null;
var viewModel;


function update() {
    $.getJSON(host + "/api/status", function(json) {

        if (espState == null) {
            espState = json;
            viewModel = ko.mapping.fromJS(espState);
            ko.applyBindings(viewModel);

            // TODO make this work
            //$("button").click(function () {
            //    togglePin(ko.dataFor(this).id());
            //});

        } else {

            espState = json;
            ko.mapping.fromJS(espState, viewModel);
        }
    });
    setTimeout(update, 1000);
}

function togglePin(pin) {
    $.get(host + "/api/gpio/write?pin=" + pin + "&state=-1", function(data, status){
        //alert(data); // TODO: some notification
    });
}

update();

//viewModel.pins()[0].name("Yeppie");

// Every time data is received from the server:
//ko.mapping.fromJS(data, viewModel);
